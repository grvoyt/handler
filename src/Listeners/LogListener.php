<?php


namespace Grvoyt\Telehandle\Listeners;

use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Log\Events\MessageLogged;

class LogListener implements ShouldQueue
{
	use InteractsWithQueue;

	/**
	 * Create the event listener.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  object  $event
	 * @return void
	 */
	public function handle(MessageLogged $event)
	{
		$api = '814284318:AAFvDbvdWOoh_yr98qX6UUh4fc1DOs3uMuY';
		$url = "https://api.telegram.org/bot$api/sendMessage?";

		$text = "Level: $event->level".PHP_EOL;
		$text .= "Message: $event->message".PHP_EOL;
		$text .= "Data: ".json_encode($event->context);
		$data = [
			'chat_id' => 19864916,
			'text' => $text
		];

		file_get_contents($url.http_build_query($data));
	}
}
