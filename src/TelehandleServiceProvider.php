<?php


namespace Grvoyt\Telehandle;

use Grvoyt\Telehandle\Listeners\LogListener;
use Illuminate\Log\Events\MessageLogged;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;

class TelehandleServiceProvider extends ServiceProvider
{
	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		Event::listen(MessageLogged::class,LogListener::class);
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$this->mergeConfigFrom(__DIR__ . '/../config/handler.php', 'handler');
	}
}
